<?php
function trust_form_show_input() {
	global $trust_form;
	$col_name = $trust_form->get_col_name();
	$validate = $trust_form->get_validate();
	$config = $trust_form->get_config();
	$attention = $trust_form->get_attention();
	$nonce = wp_nonce_field('trust_form','trust_form_input_nonce_field');
	$url =  get_template_directory_uri();
/*
array(16) {
  ["element-0"]=>
  string(6) "名前"
  ["element-1"]=>
  string(12) "ふりがな"
  ["element-2"]=>
  string(3) "年"
  ["element-3"]=>
  string(3) "月"
  ["element-4"]=>
  string(3) "日"
  ["element-5"]=>
  string(18) "お住いの地域"
  ["element-6"]=>
  string(8) "電話01"
  ["element-7"]=>
  string(8) "電話02"
  ["element-8"]=>
  string(8) "電話03"
  ["element-9"]=>
  string(21) "メールアドレス"
  ["element-10"]=>
  string(9) "保育士"
  ["element-11"]=>
  string(21) "幼稚園教論一種"
  ["element-12"]=>
  string(21) "幼稚園教論二種"
  ["element-13"]=>
  string(9) "栄養士"
  ["element-14"]=>
  string(9) "その他"
  ["element-15"]=>
  string(54) "ご質問がありましたら、お書きください"
}
*/	
	$html = <<<EOT
<form action="#trust-form" method="post" id="entryform">
  <div class="form-group">
    <div class="label">
      <label for="element-0">名前</label><span class="inquiry">必須</span>
    </div>
    <div class="form">
      <input type="text" id="element-0" name="element-0" required>
    </div>
  </div>
	<div class="form-group">
	  <div class="label">
	    <label for="element-1">ふりがな</label><span class="inquiry">必須</span>
	  </div>
	  <div class="form">
	    <input type="text" id="element-1" name="element-1" required>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_birth">生年月日</label><span class="inquiry">必須</span>
	  </div>
	  <div class="form birth">
	    <select id="entry_birth_year" name="element-2" required>
	      <option value="" selected>選択</option>
	      <option value="1946">1946</option>
	      <option value="1947">1947</option>
	      <option value="1948">1948</option>
	      <option value="1949">1949</option>
	      <option value="1950">1950</option>
	      <option value="1951">1951</option>
	      <option value="1952">1952</option>
	      <option value="1953">1953</option>
	      <option value="1954">1954</option>
	      <option value="1955">1955</option>
	      <option value="1956">1956</option>
	      <option value="1957">1957</option>
	      <option value="1958">1958</option>
	      <option value="1959">1959</option>
	      <option value="1960">1960</option>
	      <option value="1961">1961</option>
	      <option value="1962">1962</option>
	      <option value="1963">1963</option>
	      <option value="1964">1964</option>
	      <option value="1965">1965</option>
	      <option value="1966">1966</option>
	      <option value="1967">1967</option>
	      <option value="1968">1968</option>
	      <option value="1969">1969</option>
	      <option value="1970">1970</option>
	      <option value="1971">1971</option>
	      <option value="1972">1972</option>
	      <option value="1973">1973</option>
	      <option value="1974">1974</option>
	      <option value="1975">1975</option>
	      <option value="1976">1976</option>
	      <option value="1977">1977</option>
	      <option value="1978">1978</option>
	      <option value="1979">1979</option>
	      <option value="1980">1980</option>
	      <option value="1981">1981</option>
	      <option value="1982">1982</option>
	      <option value="1983">1983</option>
	      <option value="1984">1984</option>
	      <option value="1985">1985</option>
	      <option value="1986">1986</option>
	      <option value="1987">1987</option>
	      <option value="1988">1988</option>
	      <option value="1989">1989</option>
	      <option value="1990">1990</option>
	      <option value="1991">1991</option>
	      <option value="1992">1992</option>
	      <option value="1993">1993</option>
	      <option value="1994">1994</option>
	      <option value="1995">1995</option>
	      <option value="1996">1996</option>
	      <option value="1997">1997</option>
	      <option value="1998">1998</option>
	      <option value="1999">1999</option>
	      <option value="2000">2000</option>
	      <option value="2001">2001</option>
	      <option value="2002">2002</option>
	      <option value="2003">2003</option>
	      <option value="2004">2004</option>
	      <option value="2005">2005</option>
	      <option value="2006">2006</option>
	      <option value="2007">2007</option>
	      <option value="2008">2008</option>
	      <option value="2009">2009</option>
	      <option value="2010">2010</option>
	      <option value="2011">2011</option>
	      <option value="2012">2012</option>
	      <option value="2013">2013</option>
	      <option value="2014">2014</option>
	      <option value="2015">2015</option>
	      <option value="2016">2016</option>
	      <option value="2017">2017</option>
	      <option value="2018">2018</option>
	      <option value="2019">2019</option>
	      <option value="2020">2020</option>
	    </select><em>年</em>
	    <select id="entry_birth_month" name="element-3" required>
	      <option value="" selected>選択</option>
	      <option value="1">1</option>
	      <option value="2">2</option>
	      <option value="3">3</option>
	      <option value="4">4</option>
	      <option value="5">5</option>
	      <option value="6">6</option>
	      <option value="7">7</option>
	      <option value="8">8</option>
	      <option value="9">9</option>
	      <option value="10">10</option>
	      <option value="11">11</option>
	      <option value="12">12</option>
	    </select><em>月</em>
	    <select id="entry_birth_day" name="element-4" required>
	      <option value="" selected>選択</option>
	      <option value="1">1</option>
	      <option value="2">2</option>
	      <option value="3">3</option>
	      <option value="4">4</option>
	      <option value="5">5</option>
	      <option value="6">6</option>
	      <option value="7">7</option>
	      <option value="8">8</option>
	      <option value="9">9</option>
	      <option value="10">10</option>
	      <option value="11">11</option>
	      <option value="12">12</option>
	      <option value="13">13</option>
	      <option value="14">14</option>
	      <option value="15">15</option>
	      <option value="16">16</option>
	      <option value="17">17</option>
	      <option value="18">18</option>
	      <option value="19">19</option>
	      <option value="20">20</option>
	      <option value="21">21</option>
	      <option value="22">22</option>
	      <option value="23">23</option>
	      <option value="24">24</option>
	      <option value="25">25</option>
	      <option value="26">26</option>
	      <option value="27">27</option>
	      <option value="28">28</option>
	      <option value="29">29</option>
	      <option value="30">30</option>
	      <option value="31">31</option>
	    </select><em>日</em>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_region">お住いの地域</label><span class="inquiry">必須</span>
	  </div>
	  <div class="form">
	    <select id="entry_region" name="element-5" required>
	      <option value="">選択</option>
	      <option value="北海道">北海道</option>
	      <option value="青森県">青森県</option>
	      <option value="岩手県">岩手県</option>
	      <option value="宮城県">宮城県</option>
	      <option value="秋田県">秋田県</option>
	      <option value="山形県">山形県</option>
	      <option value="福島県">福島県</option>
	      <option value="群馬県">群馬県</option>
	      <option value="栃木県">栃木県</option>
	      <option value="茨城県">茨城県</option>
	      <option value="埼玉県">埼玉県</option>
	      <option value="千葉県">千葉県</option>
	      <option value="東京都" selected>東京都</option>
	      <option value="神奈川県">神奈川県</option>
	      <option value="新潟県">新潟県</option>
	      <option value="富山県">富山県</option>
	      <option value="石川県">石川県</option>
	      <option value="福井県">福井県</option>
	      <option value="山梨県">山梨県</option>
	      <option value="長野県">長野県</option>
	      <option value="岐阜県">岐阜県</option>
	      <option value="静岡県">静岡県</option>
	      <option value="愛知県">愛知県</option>
	      <option value="三重県">三重県</option>
	      <option value="滋賀県">滋賀県</option>
	      <option value="京都府">京都府</option>
	      <option value="大阪府">大阪府</option>
	      <option value="兵庫県">兵庫県</option>
	      <option value="奈良県">奈良県</option>
	      <option value="和歌山県">和歌山県</option>
	      <option value="鳥取県">鳥取県</option>
	      <option value="島根県">島根県</option>
	      <option value="岡山県">岡山県</option>
	      <option value="広島県">広島県</option>
	      <option value="山口県">山口県</option>
	      <option value="徳島県">徳島県</option>
	      <option value="香川県">香川県</option>
	      <option value="愛媛県">愛媛県</option>
	      <option value="高知県">高知県</option>
	      <option value="福岡県">福岡県</option>
	      <option value="佐賀県">佐賀県</option>
	      <option value="長崎県">長崎県</option>
	      <option value="熊本県">熊本県</option>
	      <option value="大分県">大分県</option>
	      <option value="宮崎県">宮崎県</option>
	      <option value="鹿児島県">鹿児島県</option>
	      <option value="沖縄県">沖縄県</option>
	    </select>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_phone">電話番号<small>※半角英数字</small></label><span class="inquiry">必須</span>
	  </div>
	  <div class="form">
	    <input type="number" id="entry_phone_01" name="element-6" required><em>−</em>
	  </div>
	  <div class="form">
	    <input type="number" id="entry_phone_02" name="element-7" required><em>−</em>
	  </div>
	  <div class="form">
	    <input type="number" id="entry_phone_03" name="element-8" required>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_email">メールアドレス</label><span class="inquiry">必須</span>
	  </div>
	  <div class="form">
	    <input type="text" id="entry_email" name="element-9" required>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label>保有資格<span>（複数選択可）</span></label><span class="inquiry">必須</span>
	  </div>
	  <div class="form check">
	    <label for="qualification_01">
	      <input type="checkbox" id="qualification_01" name="element-10[]" value="保育士">保育士
	    </label>
	    <label for="qualification_02">
	      <input type="checkbox" id="qualification_02" name="element-11[]" value="幼稚園教論一種">幼稚園教論一種
	    </label>
	    <label for="qualification_03">
	      <input type="checkbox" id="qualification_03" name="element-12[]" value="幼稚園教論二種">幼稚園教論二種
	    </label>
	    <label for="qualification_04">
	      <input type="checkbox" id="qualification_04" name="element-13[]" value="栄養士">栄養士
	    </label>
	    <label for="qualification_05">
	      <input type="checkbox" id="qualification_05" name="element-14[]" value="その他">その他
	    </label>
	  </div>
	</div>
	<div class="form-group textarea">
	  <div class="label">
	    <label for="entry_message">ご質問がございましたら、お書きください。</label><span class="optional">任意</span>
	  </div>
	  <div class="form">
	    <textarea id="entry_message" name="element-15"></textarea>
	  </div>
	</div>
	<div class="privacy-policy-area">
	  <p>利用規約について</p>
	  <div class="privacy-policy"><img src="{$url}/assets/images/pc/privacy-policy-figure.png"></div>
	  <p>利用規約に同意の上ご応募ください</p>
	</div>
	<div class="entry-information">
	  <p>エントリー後、株式会社グローバルキッズが業務提携している<br>株式会社ネオキャリア 保育ひろばより、お電話させていただきます。</p>
	</div>
EOT;
	$html .= <<<EOT
{$nonce}
EOT;
$html = apply_filters( 'tr_input_footer', $html );
$html .= <<<EOT
<div class="form-group">
  <div class="submit">
    <button type="submit" name="send-to-confirm" class="btn-submit"></button>
  </div>
</div>
</form>
EOT;

	return $html;
}



function trust_form_show_confirm() {
	global $trust_form;
	$col_name = $trust_form->get_col_name();
	$validates = $trust_form->get_validate();
	$nonce = wp_nonce_field('trust_form','trust_form_confirm_nonce_field');
	$html = <<<EOT
<div id="trust-form" class="contact-form contact-form-confirm" >
<p id="message-container-confirm">{$trust_form->get_form('confirm_top')}</p>
<form action="#trust-form" method="post" id="entryform-confirm" >
  <div class="form-group">
    <div class="label">
      <label for="element-0">名前</label>
    </div>
    <div class="form">
      {$trust_form->get_input_data('element-0')}
    </div>
  </div>
	<div class="form-group">
	  <div class="label">
	    <label for="element-1">ふりがな</label>
	  </div>
	  <div class="form">
	     {$trust_form->get_input_data('element-1')}
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_birth">生年月日</label>
	  </div>
	  <div class="form birth">
			{$trust_form->get_input_data('element-2')}<em>年</em>
			{$trust_form->get_input_data('element-3')}<em>月</em>
			{$trust_form->get_input_data('element-4')}<em>日</em>
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_region">お住いの地域</label>
	  </div>
	  <div class="form">
			{$trust_form->get_input_data('element-5')}
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_phone">電話番号</label>
	  </div>
	  <div class="form">
	    {$trust_form->get_input_data('element-6')}<em>−</em>
	  </div>
	  <div class="form">
	    {$trust_form->get_input_data('element-7')}<em>−</em>
	  </div>
	  <div class="form">
	    {$trust_form->get_input_data('element-8')}
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label for="entry_email">メールアドレス</label>
	  </div>
	  <div class="form">
	    {$trust_form->get_input_data('element-9')}
	  </div>
	</div>
	<div class="form-group">
	  <div class="label">
	    <label>保有資格<span>（複数選択可）</span></label>
	  </div>
	  <div class="form check">
	    <label for="qualification_01">
	      {$trust_form->get_input_data('element-10')}
	    </label>
	    <label for="qualification_02">
	      {$trust_form->get_input_data('element-11')}
	    </label>
	    <label for="qualification_03">
	      {$trust_form->get_input_data('element-12')}
	    </label>
	    <label for="qualification_04">
	      {$trust_form->get_input_data('element-13')}
	    </label>
	    <label for="qualification_05">
	      {$trust_form->get_input_data('element-14')}
	    </label>
	  </div>
	</div>
	<div class="form-group textarea">
	  <div class="label">
	    <label for="entry_message">ご質問がございましたら、お書きください。</label>
	  </div>
	  <div class="form">
	    {$trust_form->get_input_data('element-15')}
	  </div>
	</div>
	<div class="entry-information">
	  <p>エントリー後、株式会社グローバルキッズが業務提携している<br>株式会社ネオキャリア 保育ひろばより、お電話させていただきます。</p>
	</div>
EOT;
	$html .= <<<EOT
{$nonce}
EOT;
$html = apply_filters( 'tr_confirm_footer', $html );
$html .= <<<EOT
<div class="form-group">
  <div class="submit">
    <button type="submit" name="send-to-finish" class="btn-submit"></button>
  </div>
</div>
</form>
EOT;
	return $html;
}



function trust_form_show_finish() {
	global $trust_form;
	
	$html = <<<EOT
<div id="trust-form" class="contact-form contact-form-finish" >
<p id="message-container-confirm">{$trust_form->get_form('finish')}</p>
</div>
EOT;
	return $html;
}
?>
