<?php
/**
 * recruit-hoikushi functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package recruit-hoikushi
 */

if ( ! function_exists( 'recruit_hoikushi_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function recruit_hoikushi_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on recruit-hoikushi, use a find and replace
	 * to change 'recruit-hoikushi' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'recruit-hoikushi', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	// add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	//add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'recruit-hoikushi' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'recruit_hoikushi_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'recruit_hoikushi_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function recruit_hoikushi_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'recruit_hoikushi_content_width', 640 );
}
add_action( 'after_setup_theme', 'recruit_hoikushi_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function recruit_hoikushi_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'recruit-hoikushi' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'recruit-hoikushi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'recruit_hoikushi_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function recruit_hoikushi_scripts() {
	if (is_page('lp2')) {
		wp_enqueue_style( 'recruit-hoikushi-lp-ver4-style', get_template_directory_uri() .'/assets/css/style_ver3.css');
		wp_enqueue_script( 'recruit-hoikushi-lp-ver3-script-smooth', get_template_directory_uri() .'/assets/js/smoothScroll.js');
		wp_enqueue_script( 'recruit-hoikushi-lp-ver3-script-tooltip', get_template_directory_uri() .'/assets/js/showTooltip.js');
		wp_enqueue_script( 'recruit-hoikushi-jquery-validate', get_template_directory_uri() .'/assets/js/jquery.validate.min.js', array(), '20151215', true );
	} else if (is_page('lp3')) {
		wp_enqueue_style( 'recruit-hoikushi-lp-ver4-style', get_template_directory_uri() .'/assets/css/style_ver4.css');
		wp_enqueue_script( 'recruit-hoikushi-lp-ver3-script-smooth', get_template_directory_uri() .'/assets/js/smoothScroll.js');
		wp_enqueue_script( 'recruit-hoikushi-lp-ver3-script-tooltip', get_template_directory_uri() .'/assets/js/showTooltip.js');
		wp_enqueue_script( 'recruit-hoikushi-jquery-validate', get_template_directory_uri() .'/assets/js/jquery.validate.min.js', array(), '20151215', true );
	} else {

		wp_enqueue_style( 'recruit-hoikushi-style', get_stylesheet_uri() );
		wp_enqueue_style( 'recruit-hoikushi-normalize-style', get_template_directory_uri() .'/assets/css/normalize.css');
		wp_enqueue_style( 'recruit-hoikushi-basic-style', get_template_directory_uri() .'/assets/css/basics.css');
		wp_enqueue_style( 'recruit-hoikushi-main-style', get_template_directory_uri() .'/assets/css/styles.css');
	 	wp_enqueue_style( 'recruit-hoikushi-mobile-style', get_template_directory_uri() .'/assets/css/mobile.css');
		wp_enqueue_style( 'recruit-hoikushi-section-scroll-style', get_template_directory_uri() .'/assets/css/section-scroll.css');
		wp_enqueue_style( 'recruit-hoikushi-lp-main', get_template_directory_uri() .'/assets/css/main.css');
		wp_enqueue_style( 'recruit-hoikushi-lp-style_pc', get_template_directory_uri() .'/assets/css/style_pc.css');
		wp_enqueue_style( 'recruit-hoikushi-lp-style_sp', get_template_directory_uri() .'/assets/css/style_sp.css');

		if(is_front_page()) {
			wp_enqueue_script( 'recruit-hoikushi-jquery-section-scroll', get_template_directory_uri() .'/assets/js/jquery.section-scroll.js', array(), '20151215', true );
			wp_enqueue_script( 'recruit-hoikushi-jquery-validate', get_template_directory_uri() .'/assets/js/jquery.validate.min.js', array(), '20151215', true );
			wp_enqueue_script( 'recruit-hoikushi-index', get_template_directory_uri() .'/assets/js/index.js', array(), '20151215', true );
		}
		wp_enqueue_script( 'recruit-hoikushi-lp_index', get_template_directory_uri() .'/assets/js/lp_index.js', array(), '20151215', true );
	}
	wp_enqueue_script( 'recruit-hoikushi-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), '20151215', true );
	wp_enqueue_script( 'recruit-hoikushi-jquery-easing', get_template_directory_uri() .'/assets/js/jquery.easing.1.3.js', array(), '20151215', true );
	wp_enqueue_script( 'recruit-hoikushi-carousel', get_template_directory_uri() .'/assets/js/carousel.js', array(), '20151215', true );

	remove_action('wp_head','wp_oembed_add_host_js');
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'recruit_hoikushi_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Remove Emoji
function disable_emoji() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

function debug($var) {
	echo "<pre>\n";
	var_dump($var);
	echo "</pre>\n";
}

remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

function theme_name_scripts() {
 wp_enqueue_style( 'validationEngine.jquery.css', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css', array(), '1.0', 'all');
 wp_enqueue_script( 'jquery.validationEngine-ja.js', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-ja.min.js', array('jquery'), '2.0.0', true );
 wp_enqueue_script( 'jquery.validationEngine.js', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true );
}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
