<?php
if (is_page('lp3') || is_page('entry-finish-lp')) {
  $title = "保育士の転職・求人ページ";
} else {
  $title = get_bloginfo('name').' | '.get_bloginfo('description');
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<meta name="description" content="">
<meta name="keyword" content="">
<meta name="author" content="">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo get_bloginfo('url'); ?>">
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/og_image.jpg">
<meta property="og:site_name" content="<?php echo $title; ?>">
<meta property="og:description" content="">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="alternate" type="application/rss+xml" title="<?php echo $title; ?>" href="<?php echo get_bloginfo('url') . '/feed/'; ?>">
<link rel="alternate" type="application/rss+xml" title="<?php echo $title; ?>" href="<?php echo get_bloginfo('url') . '/comments/feed/'; ?>">
<?php wp_head(); ?>
<script>
jQuery(document).ready(function(){
 jQuery('.screen-reader-response').remove();
 jQuery('#form_step_4_sp .form_message .form_step_menu.sp').children('p').remove();
 jQuery("#entry_name").addClass("validate[required]");
 jQuery("#entry_hurigana").addClass("validate[required]");
 jQuery("#entry_birth_year").addClass("validate[required]");
 jQuery("#entry_birth_month").addClass("validate[required]");
 jQuery("#entry_birth_day").addClass("validate[required]");
 jQuery("#entry_region").addClass("validate[required]");
 jQuery("#entry_phone_01").addClass("validate[required,maxSize[4],custom[number]]");
 jQuery("#entry_phone_02").addClass("validate[required,maxSize[4],custom[number]]");
 jQuery("#entry_phone_03").addClass("validate[required,maxSize[4],custom[number]]");
 jQuery("#entry_email").addClass("validate[required,custom[email]]");
 //jQuery("#entry_name_sp").addClass("validate[required]");
 //jQuery("#entry_hurigana_sp").addClass("validate[required]");
 //jQuery("#entry_birth_year_sp").addClass("validate[required]");
 //jQuery("#entry_birth_month_sp").addClass("validate[required]");
 //jQuery("#entry_birth_day_sp").addClass("validate[required]");
 //jQuery("#entry_region_sp").addClass("validate[required]");
 //jQuery("#entry_phone_01_sp").addClass("validate[required,maxSize[4],custom[number]]");
 //jQuery("#entry_phone_02_sp").addClass("validate[required,maxSize[4],custom[number]]");
 //jQuery("#entry_phone_03_sp").addClass("validate[required,maxSize[4],custom[number]]");
 //jQuery("#entry_email_sp").addClass("validate[required,custom[email]]");
 jQuery("#entry_check label span").addClass("checkbox");
 jQuery("#entry_check_sp label span").addClass("checkbox");
 jQuery("#idForm").validationEngine();
 jQuery("#idForm_sp").validationEngine();
});
</script>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WDQ57B"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WDQ57B');</script>
<!-- End Google Tag Manager -->
<?php if(is_page('new') || is_page('privacy-policy') || is_page('entry-finish')) : ?>
<div id="header">
  <div class="scrollable-section">
    <div class="wrapper">
      <div id="main-logo">
        <h1 class="ind">株式会社グローバルキッズ</h1>
      </div>
      <div id="header-sub-message" class="hide-sp">
        <p>採用ページ</p>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
