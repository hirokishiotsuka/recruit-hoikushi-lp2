<?php
if (is_page('new') || is_page('privacy-policy') || is_page('entry-finish')) {
?>
<footer class="footer">
  このページは株式会社グローバルキッズと業務提携している株式会社ネオキャリアが運営しています。
</footer>
<?php
} elseif (is_page('entry-finish-lp')) {
?>
<footer class="footer">
  Copyright (C) Neo Career CO., LTD. All rights reserved.
</footer>
<?php
}
?>
<?php wp_footer(); ?>
</body>
</html>
